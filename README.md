# daeds-datepicker

A minimal, simple and lightweight datepicker for React.

## Features / limitations

* Only supports yyyy-mm-dd date format
* Other date formats might be supported in the future
* Falls back to native date input on mobile
* Min and max date will be supported in the future