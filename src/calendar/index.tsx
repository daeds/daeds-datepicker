import React, { useState } from "react";
import "./index.css";
import { formatDate, getMonthInfo } from "../utils";

const months = ["Januari", "Februari", "Mars", "April", "Maj", "Juni", "Juli", "Augusti", "September", "Oktober", "November", "December"];
const days = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31];
const weekDays = [1, 2, 3, 4, 5, 6];
const dayHeaders = ["Må", "Ti", "On", "To", "Fr", "Lö", "Sö"];

interface CalendarProps {
    selectedDate?: string;
    onChange: (date: string) => void;
    className?: string;
}

export const Calendar = function ({ selectedDate, onChange, className }: CalendarProps) {
    const formattedDate = formatDate(selectedDate, true);
    const [date, setDate] = useState(formattedDate ? new Date(formattedDate) : new Date());
    const dateTmp = new Date(date.getTime());
    const header = `${months[dateTmp.getMonth()]} ${dateTmp.getFullYear()}`;
    const info = getMonthInfo(dateTmp, formattedDate);

    return (
        <div className={`datepicker__calendar ${className || ""}`}>
            <div className="datepicker__calendar-header">
                <div className="datepicker__calendar-header-text">{header}</div>
                <button className="datepicker__calendar-header-button" onClick={_ => setDate(new Date(date.setMonth(date.getMonth() - 1)))} title="Föregående månad">&#9668;</button>
                <button className="datepicker__calendar-header-button" onClick={_ => setDate(new Date(date.setMonth(date.getMonth() + 1)))} title="Nästa månad">&#9658;</button>
            </div>
            <div className="datepicker__calendar-tbl">
                {dayHeaders.map(day => <div key={day} className="datepicker__calendar-cell datepicker__calendar-cell--header">{day}</div>)}
                {weekDays.map(d => d < info.firstDay! ? <div key={d} className="datepicker__calendar-cell"></div> : null)}
                {days.map(d => d <= info.totalDays
                    ? <button
                        key={d} 
                        onClick={_ => onChange((new Date(date.setDate(d)).toLocaleDateString("sv")))} 
                        className={`datepicker__calendar-cell datepicker__calendar-cell-button  ${d === info.selected ? "datepicker__calendar-cell--selected" : ""}`}>
                            {d}
                    </button>
                    : null
                )}
            </div>
        </div>
    );
};