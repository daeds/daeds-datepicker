import React from "react";
import { Calendar } from "."
import { storiesOf } from "@storybook/react";

storiesOf("Calendar", module)
  .add("Default", () => (
    <Calendar onChange={alert} />
  ))
  .add("With preselected date", () => {
    const [selected, setSelected] = React.useState("2019-06-21");

    return (
      <>
        <Calendar selectedDate={selected} onChange={setSelected} />
        <div style={{marginTop: "20px"}}>Selected date: {selected}</div>
      </>
    );
  })