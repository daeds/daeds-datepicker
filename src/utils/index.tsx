import { useEffect, MutableRefObject } from "react";

type OutSideClickEvt = TouchEvent | MouseEvent;

interface DateInfo {
   firstDay?: number;
   totalDays?: number;
   selected?: number;
}

export const useOutsideClick = (
   ref: MutableRefObject<any>,
   handler: (e: OutSideClickEvt) => any
) => {
   const handleClick = (e: OutSideClickEvt) => {
      if (!ref.current) return;

      if (!ref.current.contains(e.target)) {
         handler(e);
      }
   };

   useEffect(() => {
      document.addEventListener("touchstart", handleClick);
      document.addEventListener("mousedown", handleClick);

      return () => {
         document.removeEventListener("touchstart", handleClick);
         document.removeEventListener("mousedown", handleClick);
      };
   }, [ref, handler]);

   return ref;
};

export const getMonthInfo = (d: Date, selectedDate?: string): DateInfo => {
   let result: DateInfo = {};
   d.setHours(0, 0, 0, 0);
   d.setDate(1);

   const first = d.toLocaleDateString("sv");

   result.firstDay = d.getDay();
   result.firstDay = result.firstDay === 0 ? 7 : result.firstDay;
   d.setMonth(d.getMonth() + 1);
   d.setDate(d.getDate() - 1);
   result.totalDays = d.getDate();

   const last = d.toLocaleDateString("sv");

   if (selectedDate <= last && selectedDate >= first) {
       result.selected = parseInt(selectedDate.substr(selectedDate.length - 2), 10);
   }

   return result;
};

const mobileRegex = /android|iPhone|iPad|iPod/i;
export const isMobile = () => mobileRegex.test(navigator.userAgent);

export const formatDate = (date: string | Date, returnEmptyIfInvalid?: boolean): string => {
   if (!date) {
      return "";
   }

   if (date instanceof Date) {
      return date.toLocaleDateString("sv");
   }

   let digits = date.replace(/[^\d]/g, "");

   if (digits.length === 6) {
      const add19Century = (parseInt(digits.substr(0,2), 10)) - parseInt((new Date().getFullYear() + "").substr(0,2), 10) > 10;
      digits = (add19Century ? "19" : "20") + digits;
   }

   if (digits.length !== 8) {
      return returnEmptyIfInvalid ? "" : date;
   }

   const dateObj = new Date(`${digits.substr(0, 4)}-${digits.substr(4, 2)}-${digits.substr(6, 2)}`);

   if (dateObj.getTime() !== dateObj.getTime()) {
      return returnEmptyIfInvalid ? "" : date;
   }

   return dateObj.toLocaleDateString("sv");
};

export const isDate = (date: string, emptyIsOk?: boolean) => (!date && emptyIsOk) || formatDate(date, true) !== "";
