import { formatDate, getMonthInfo, isDate } from "."

Date.prototype.toLocaleDateString = function () {
    return `${this.getFullYear()}-${(this.getMonth() < 9 ? "0" : "") + (this.getMonth() + 1)}-${(this.getDate() < 10 ? "0" : "") + this.getDate()}`;
};

describe("utils", () => {

    describe("formatDate", () => {
        it("should handle Date object", () =>
            expect(formatDate(new Date("2020-09-10"))).toBe("2020-09-10"));

        it("should handle undefined/null", () =>
            expect(formatDate(undefined)).toBe(""));

        it("should add 20XX century for 6-digit date if year is below current year", () =>
            expect(formatDate("190501")).toBe("2019-05-01"));

        it("should add 20XX century for 6-digit date if year is max 10 above current year", () =>
            expect(formatDate(((new Date().getFullYear() + 10) + "").substr(2) + "0501")).toBe("2030-05-01"));

        it("should add 19XX century for 6-digit date if year is more than 10 above current year", () =>
            expect(formatDate(((new Date().getFullYear() + 11) + "").substr(2) + "0501")).toBe("1931-05-01"));

        it("should handle 8-digit date", () =>
            expect(formatDate("20200815")).toBe("2020-08-15"));

        it("should handle already formatted date", () =>
            expect(formatDate("2020-08-20")).toBe("2020-08-20"));

        it("should return original string if invalid date", () =>
            expect(formatDate("abc")).toBe("abc"));

        it("should be able to return empty if invalid date", () =>
            expect(formatDate("abc", true)).toBe(""));

        it("should handle leap year", () =>
            expect(formatDate("20200229")).toBe("2020-02-29"));

        it("should only care about the digits in string", () =>
            expect(formatDate("2019/02-,-12")).toBe("2019-02-12"));

        it("should handle invalid month (too high)", () =>
            expect(formatDate("20201301", true)).toBe(""));

        it("should handle invalid month (too low)", () =>
            expect(formatDate("20200001", true)).toBe(""));

        it("should handle invalid day (too high)", () =>
            expect(formatDate("20200132", true)).toBe(""));

        it("should handle invalid day (too low)", () =>
            expect(formatDate("20200100", true)).toBe(""));
    });

    describe("getMonthInfo", () => {
        it("should get the first weekday of the month", () =>
            expect(getMonthInfo(new Date("2020-05-10")).firstDay).toBe(5));

        it("should get the total number of days in the month", () =>
            expect(getMonthInfo(new Date("2020-05-10")).totalDays).toBe(31));

        it("should handle leap years", () =>
            expect(getMonthInfo(new Date("2020-02-10")).totalDays).toBe(29));

        it("should not set selected if selectedDate is outside of current month", () =>
            expect(getMonthInfo(new Date("2020-05-10"), "2020-04-30").selected).toBeUndefined());

        it("should set selected if selectedDate is in current month", () =>
            expect(getMonthInfo(new Date("2020-05-10"), "2020-05-01").selected).toBe(1));
    });

    describe("isDate", () => {
        it("should return true for valid date", () =>
            expect(isDate("2020-03-10")).toBe(true));

        it("should return false for invalid date", () =>
            expect(isDate("2020-03-45")).toBe(false));
        
        it("should return false for empty string", () =>
            expect(isDate("")).toBe(false));
        
        it("should return true for empty string if empty is ok", () =>
            expect(isDate("", true)).toBe(true));
    });
});