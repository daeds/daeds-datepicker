import React from "react";
import { DatePicker } from "."
import { storiesOf } from "@storybook/react";

storiesOf("DatePicker", module)
  .add("Default", () => {
    const [val, setVal] = React.useState("");
    
    return (
      <DatePicker value={val} onChange={setVal} />
    );
  })
  .add("With preselected date", () => {
    const [val, setVal] = React.useState(new Date().toLocaleDateString("sv"));
    
    return (
      <DatePicker value={val} onChange={setVal} />
    );
  })
  .add("With invalid date", () => {
    const [val, setVal] = React.useState("asfd");
    
    return (
      <DatePicker value={val} onChange={setVal} />
    );
  })
  .add("With multiple inputs", () => {
    const [val, setVal] = React.useState("");
    const [val2, setVal2] = React.useState("");
    
    return (
      <>
        <DatePicker value={val} onChange={setVal} /> <br/>
        <DatePicker value={val2} onChange={setVal2} /> <br/>
      </>
    );
  })