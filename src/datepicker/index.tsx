import React, { useState, useRef, useEffect } from "react";
import { Calendar } from "../calendar";
import { useOutsideClick, isMobile, formatDate } from "../utils";
import "./index.css";

interface DatePickerProps {
    onChange: (value: string) => void;
    value: string;
    className?: string;
}

export const DatePicker = ({onChange, value, className}: DatePickerProps) => {
    const [showCalendar, setShowCalendar] = useState(false);
    const wrapperDiv = useRef<HTMLDivElement>(null);
    const mobile = isMobile();
    const onCalendarChange = value => {
        setShowCalendar(false); 
        onChange(value);
    };
    const onBlur = () => {
        if (mobile) return;
        
        const formatted = formatDate(value);

        if (formatted !== value) {
            onChange(formatted);
        }
    }

    useOutsideClick(wrapperDiv, () => !mobile && setShowCalendar(false));

    return (
        <div className="datepicker__input-wrapper" ref={wrapperDiv}>
            <input
                className={className}
                onKeyDown={mobile ? undefined : e => e.which === 9 && setShowCalendar(false)}
                type={mobile ? "date" : "text"}
                onChange={e => onChange(e.target.value)} 
                value={value}
                onFocus={mobile ? undefined : () => setShowCalendar(true)}
                onBlur={onBlur} />
            
            {!mobile && showCalendar && <Calendar 
                selectedDate={value} 
                onChange={onCalendarChange}
                className="datepicker__input-active-calendar" />}
        </div>
    );
};