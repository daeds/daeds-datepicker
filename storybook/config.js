import { configure } from '@storybook/react';

function loadStories() {
  require('../src/datepicker/index.stories.tsx');
  require('../src/calendar/index.stories.tsx');
}

configure(loadStories, module);